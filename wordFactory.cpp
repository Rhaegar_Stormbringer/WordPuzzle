#include "wordFactory.h"

int getNumberLine(const string fileName)
{
    int numberLine(0);
    ifstream file(fileName.c_str());
    if (file)
    {
        string line;
        while (getline(file, line))
        {
            numberLine ++;
        }
    }
    else
    {
        cout << "ERREUR" << endl;
    }

    return numberLine;
}


string wordChoice(const string fileName)
{
    string line("");
    int numberLine = getNumberLine(fileName);

    srand(time(NULL));
    int r = rand()%(numberLine+1);
    ifstream file(fileName.c_str());
    if (file)
    {
        int i(0);
        while (i < r)
        {
            getline(file, line);
            i ++;
        }
    }
    else
    {
        cout << "ERREUR" << endl;
    }
    return line;
}


string wordShaker(string word)
{
    string newWord("");
    int r(0);
    while (word != "")
    {
        //cout << word << endl;
        r = rand()%(word.length()+1);
        //cout << r << endl;
        newWord += word[r];
        //cout << newWord << endl;
        word.erase(r, 1);
    }
    return newWord;
}
