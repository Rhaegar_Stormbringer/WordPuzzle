#ifndef WORDFACTORY_H
#define WORDFACTORY_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>

using namespace std;

int getNumberLine(const string fileName);
string wordChoice(const string fileName);
string wordShaker(string word);

#endif
