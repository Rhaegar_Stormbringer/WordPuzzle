#include "gameEngine.h"

string constructMenu(const string language)
{
    string menu("");
    string path("");
    string line("");

    /* --- [1] menu main part --- */
    path = "data/"+language+"/menu.m";
    ifstream file1(path.c_str());
    if (file1)
    {
        while(getline(file1, line))
        {
            menu += line+"\n";
        }
    }
    menu += "\n";

    /* --- [2] version & copyright --- */
    path = "data/versncop.cccp";
    ifstream file2(path.c_str());
    if (file2)
    {
        while(getline(file2, line))
        {
            menu += line+"\n";
        }
    }
    return menu;
}

vector<string> getMenuInput(const string language)
{
    vector<string> menu_input;
    string line("");
    string path("");

    path = "data/"+language+"/menu_input.m";
    ifstream file(path.c_str());
    if (file)
    {
        while(getline(file, line))
        {
            menu_input.push_back(line);
        }
    }
    return menu_input;
}


int gameLoop(const string word, const string shakedWord, int life)
{
    string answer("");
    while (life != 0)
    {
        cout << "\n---------------" << endl;
        cout << "Quel est le MOT MYSTERE cache ici : " << shakedWord << " ?" << endl;
        cin >> answer;
        if (answer == word)
        {
            return 1;
        }
        else
        {
            life -= 1;
            cout << "mauvais mot !" << endl;
            cout << "il vous reste " << life << " vie(s)" << endl;
        }
    }
    return 0;
}


int dieOrRetry ()
{
    string answer("");
    while ((answer.compare("o")) && (answer.compare("n")))
    {
        cout << "Souhaitez-vous refaire une partie ? [o/n]" << endl;
        cin >> answer;
    }
    if (answer == "o")
    {
        return 1;
    }
    return 0;
}

string getLanguage()
{
    string line("");
    ifstream file("opt.ini");
    if (file)
    {
        getline(file, line);
    }
    return line;
}


void play()
{
    cout << "ok !" << endl;
}

void option()
{
    cout << "option !" << endl;
}

int exit()
{
    return 0;
}
void menu()
{
    string language(getLanguage());
    string menu(constructMenu(language));
    vector<string> menu_input(getMenuInput(language));
    string answer("");
    cout << menu << endl;
    while ((answer.compare(menu_input[0])) && (answer.compare(menu_input[1])) && answer.compare(menu_input[2]))
    {
        cout << ">> ";
        cin >> answer;
    }
    if (!answer.compare(menu_input[0]))
    {
        play();
    }
    if (!answer.compare(menu_input[2]))
    {
        exit();
    }
}
