#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int gameLoop(const string word, const string shakedWord, int life);
int dieOrRetry();
void menu();
#endif
